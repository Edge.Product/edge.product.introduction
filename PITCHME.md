﻿
## FY18 Q2
## SFDC Practice ALL MTG
### edge product team

---?image=images/background.png

### $ edgeproduct --アジェンダ

- チームメンバー
- 活動内容と実績
- 今後の活動
- その他

---?image=images/background.png

### $ edgeproduct --チームメンバー

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png


### edge product team @ 1.0.0
### ・mibu.a.watanabe
### ・takahito.miyamoto


---?image=images/background.png

### $ edgeproduct --活動内容と実績

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

## アーリアダプターな活動を組織の中で率先しておこなうチーム

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

## 1. 最新技術動向のチェック

- heroku 調査 |
- Lightning Design System 調査 |
- sfdx 調査 |

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

## 2. 動くモノ作り

- heroku worker role アプリ作成 |
- sfdx スクラッチ組織作成 アプリ作成 |

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

## 3. 技術展開

- Lightning トレーニングキット作成 |
- heroku 勉強会 |
- git 勉強会（予定） |

---?image=images/background.png

### $ edgeproduct --今後の活動

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

会員サイトのデモ作成（2017/12～2017/1）
![heroku-demo](images/heroku-demo.png)

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

# その後は乞うご期待！

---?image=images/background.png

### $ edgeproduct --その他

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

# チームメンバー大募集！

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

- ちーむは あっとほーむ なふいんきです |
- カチャカチャ…ッターン するのが好きな人や |
- こんなときこそ 燃える やつだったハズの人 |
- XXXが一晩でやってくれました を実行できる人 |

![Press Down Key](images/down-arrow.png)

+++?image=images/background.png

# ぜひ助けてください(^q^)

---?image=images/background.png

ご清聴ありがとうございました